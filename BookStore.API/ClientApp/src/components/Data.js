
export const sliderItems = [
    {
        id: 1,
        img: "https://images.pexels.com/photos/4865746/pexels-photo-4865746.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "OPENING SALE",
        desc: "EVERYTHING 10% OFF",
        bg: "#f5faf5",
    },
    {
        id: 2,
        img: "https://images.pexels.com/photos/3772630/pexels-photo-3772630.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "LEARN ABOUT EGYPT",
        desc: "BUY TWO GET ANOTHER ONE FREE",
        bg: "#fcf1ed",
    },
    {
        id: 3,
        img: "https://images.pexels.com/photos/256450/pexels-photo-256450.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "BOOK OF THE WEEK",
        desc: "ABOUT PASSION AND MOTIVATION",
        bg: "#fbf0f4",
    }
]

export const categories = [
    {
        id: 1,
        img: "https://images.pexels.com/photos/2494701/pexels-photo-2494701.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "ROMANCE",
        desc: 'For ...'
    },
    {
        id: 2,
        img: "https://images.pexels.com/photos/3185488/pexels-photo-3185488.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "HISTORY",
        desc: 'For ...'
    },
    {
        id: 3,
        img: "https://images.pexels.com/photos/34223/mont-saint-michel-france-normandy-europe.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "FANTASY",
        desc: 'For a ...'
    }
]
export const showmore = [
    {
        image: "https://images.pexels.com/photos/768125/pexels-photo-768125.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        text: "SHOW MORE"
    }
]
export const books = [
    {
        id: 1,
        Title: "Elden Ring",
        Genre: "Fantasy",
        Desc: "Lórum ipse egy gyarigos léboltán a túl kevés és a túl sok között. Körtességek tanárok óta supoztak ehhez a garjúdhoz. A vezedés buliján a legjobban pezsgős nyakájról, a vényeres dalapétról egy szört sem töppeszt meg a zúzászban. A kilmi tüllők pacsos kolomai sakodták foszomagos vényeres dalapétát. Az apar bulátának „kozása „a nyiton, roppant zönték jutkákat torosítottak bele. Dúlékony trakák kallóztak, amelyekben a ságos buszté az alás és a gúnyarás szakomfája volt, szinte már a vizsgás csirtei szerint. De a nyúlápok nyomban renlítik, ha az őket szerzetéz dalapétban valami ernyes. Galka kulasága foszomagos vényeres taliságainál hemezett a koptájáról, a hézőséghöz fújtott és izgatottan csettetette:",
        Price: "60",
        Img: "https://image.api.playstation.com/vulcan/ap/rnd/202108/0410/0Jz6uJLxOK7JOMMfcfHFBi1D.png",
        Publisher: "FromSoft",
        Author: "Miyazaki"
    },
    {
        id: 2,
        Title: "UwU",
        Genre: "Fantasy",
        Desc: "Lórum ipse egy gyarigos léboltán a túl kevés és a túl sok között. Körtességek tanárok óta supoztak ehhez a garjúdhoz. A vezedés buliján a legjobban pezsgős nyakájról, a vényeres dalapétról egy szört sem töppeszt meg a zúzászban. A kilmi tüllők pacsos kolomai sakodták foszomagos vényeres dalapétát. Az apar bulátának „kozása „a nyiton, roppant zönték jutkákat torosítottak bele. Dúlékony trakák kallóztak, amelyekben a ságos buszté az alás és a gúnyarás szakomfája volt, szinte már a vizsgás csirtei szerint. De a nyúlápok nyomban renlítik, ha az őket szerzetéz dalapétban valami ernyes. Galka kulasága foszomagos vényeres taliságainál hemezett a koptájáról, a hézőséghöz fújtott és izgatottan csettetette:",
        Price: "14000",
        Img: "https://image.api.playstation.com/vulcan/ap/rnd/202108/0410/0Jz6uJLxOK7JOMMfcfHFBi1D.png",
        Publisher: "Heewo",
        Author: "Senpai"
    },
    {
        id: 3,
        Title: "3",
        Genre: "Fantasy",
        Desc: "Lórum ipse egy gyarigos léboltán a túl kevés és a túl sok között. Körtességek tanárok óta supoztak ehhez a garjúdhoz. A vezedés buliján a legjobban pezsgős nyakájról, a vényeres dalapétról egy szört sem töppeszt meg a zúzászban. A kilmi tüllők pacsos kolomai sakodták foszomagos vényeres dalapétát. Az apar bulátának „kozása „a nyiton, roppant zönték jutkákat torosítottak bele. Dúlékony trakák kallóztak, amelyekben a ságos buszté az alás és a gúnyarás szakomfája volt, szinte már a vizsgás csirtei szerint. De a nyúlápok nyomban renlítik, ha az őket szerzetéz dalapétban valami ernyes. Galka kulasága foszomagos vényeres taliságainál hemezett a koptájáról, a hézőséghöz fújtott és izgatottan csettetette:",
        Price: "14000",
        Img: "https://image.api.playstation.com/vulcan/ap/rnd/202108/0410/0Jz6uJLxOK7JOMMfcfHFBi1D.png",
        Publisher: "Heewo",
        Author: "d"
    },
    {
        id: 4,
        Title: "4",
        Genre: "Fantasy",
        Desc: "Lórum ipse egy gyarigos léboltán a túl kevés és a túl sok között. Körtességek tanárok óta supoztak ehhez a garjúdhoz. A vezedés buliján a legjobban pezsgős nyakájról, a vényeres dalapétról egy szört sem töppeszt meg a zúzászban. A kilmi tüllők pacsos kolomai sakodták foszomagos vényeres dalapétát. Az apar bulátának „kozása „a nyiton, roppant zönték jutkákat torosítottak bele. Dúlékony trakák kallóztak, amelyekben a ságos buszté az alás és a gúnyarás szakomfája volt, szinte már a vizsgás csirtei szerint. De a nyúlápok nyomban renlítik, ha az őket szerzetéz dalapétban valami ernyes. Galka kulasága foszomagos vényeres taliságainál hemezett a koptájáról, a hézőséghöz fújtott és izgatottan csettetette:",
        Price: "14000",
        Img: "",
        Publisher: "Heewo",
        Author: "a"
    },


]